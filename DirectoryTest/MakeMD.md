# Test
그냥 text파일을 .md로 저장했을 때 알아서 markdown문서로 변경해주는지가 궁금함

# 결과
알아서 markdown으로 변경해주었음  
하지만 Preview를 보면서 문서를 작성하고 싶다면 .md파일 생성 후 edit해주어야함 