# GitStudy

git을 공부하고 테스트해보는 저장소

# 테스트해보고 싶은 것들
## Branch
- 새로 생성: 아무 파일도 없이 새로운 branch를 만든다
- base에서 분기: base branch에서 복사해와서 branch를 분기한다
- 원격/로컬 branch 삭제
- branch 이름 바꾸기

## Commit
- Commit 취소
- Commit Message 변경
- Commit 이력 삭제

## Pull
- pull과 fetch의 차이

## Push

## Stash

## Merge
- Merge와 Rebase 차이
- 

# 참고자료
- 자주 사용하는 명령어: https://link.medium.com/jUGimvmaY4
- 브랜치 명명규칙: https://rumblefish.tistory.com/65
